function updateTimer()
{
    var timer = document.getElementById("timer");

    var tag = new Date('2019-02-16T13:00:00');
    var date = new Date();

    var diff = tag - date;
    var diff_sec = Math.floor(diff / 1000);

    var day = Math.floor(diff_sec / 24/ 60 / 60);
 
    diff_sec = diff_sec - day * 24 * 60 * 60;
    var hour = Math.floor(diff_sec / 60 /60);
 
    diff_sec = diff_sec - hour * 60 * 60;
    var min = Math.floor(diff_sec / 60);

    diff_sec = diff_sec - min * 60;
    var sec = diff_sec;

    timer.textContent = ('0' + day).slice(-2) + ":" + ('0' + hour).slice(-2) + ":" + ('0' + min).slice(-2) + ":" + ('0' + sec).slice(-2);
}
updateTimer();
setInterval("updateTimer()", 1000);
